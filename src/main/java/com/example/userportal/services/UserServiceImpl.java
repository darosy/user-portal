package com.example.userportal.services;

import com.example.userportal.model.UserModel;

import java.util.List;

public interface UserServiceImpl {
    void create(UserModel user);

    void delete(Long id);

    List<UserModel> findAll();

    UserModel findById(Long id);

    void update(Long id, UserModel user);
}
