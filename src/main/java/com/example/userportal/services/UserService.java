package com.example.userportal.services;

import com.example.userportal.model.UserModel;
import com.example.userportal.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserServiceImpl{

    @Autowired
    private UserRepository repository;

    @Override
    public void create(UserModel user) {
        user.setId(System.currentTimeMillis());
        repository.save(user);
    }

    @Override
    public void delete(Long id) {
        UserModel user = findById(id);
        repository.delete(user);
    }

    @Override
    public List<UserModel> findAll() {
        return repository.findAll();
    }

    @Override
    public UserModel findById(Long id) {
        Optional<UserModel> user = repository.findById(id);
        UserModel userModel = null;
        if (user != null)
            userModel = user.get();
        return userModel;
    }

    @Override
    public void update(Long id, UserModel user) {
        UserModel userModel = findById(id);
        userModel.setFirstName(user.getFirstName());
        userModel.setLastName(user.getLastName());
        userModel.setEmail(user.getEmail());
        userModel.setPassword(user.getPassword());
        repository.saveAndFlush(userModel);
    }
}
