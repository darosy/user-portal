package com.example.userportal.model;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "tbl_user_test")
public class UserModel {

    @Id
    @Column(name = "id_user")
    private Long id;

    @Column(name = "first_name", length = 50)
    @Size(max = 50)
    private String firstName;

    @Column(name = "last_name", length = 50)
    @Size(max = 50)
    private String lastName;

    @Column(length = 30, unique = true)
    @Email
    @Size(max = 30)
    private String email;

    @Column(length = 16)
    @Size(max = 16)
    private String password;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
