package com.example.userportal.controller;

import com.example.userportal.model.UserModel;
import com.example.userportal.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({"/api"})
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody UserModel user){
        userService.create(user);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @GetMapping(path = {"/{id}"})
    public UserModel findOne(@PathVariable("id") Long id){
        return userService.findById(id);
    }

    @PutMapping(path = {"/{id}"})
    public ResponseEntity<Void> update(@PathVariable("id") Long id, @RequestBody UserModel user){
        userService.update(id, user);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @DeleteMapping(path ={"/{id}"})
    public void delete(@PathVariable("id") Long id) {
        userService.delete(id);
    }

    @GetMapping
    public List<UserModel> findAll(){
        return userService.findAll();
    }
}
